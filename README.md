# Latest Build Status

## Library

| repository name | Windows | MacOS | Linux |
|-----------------|---------|-------|-------|
| pygncd          | [![Build status](https://ci.appveyor.com/api/projects/status/emi6f8jtaihf3tc8?svg=true)](https://ci.appveyor.com/project/zhaofeng-shu33/pygncd)|  [![Travis (.com)](https://img.shields.io/travis/com/zhaofeng-shu33/pygncd)](https://travis-ci.com/zhaofeng-shu33/pygncd)     |  [![Travis (.com)](https://img.shields.io/travis/com/zhaofeng-shu33/pygncd)](https://travis-ci.com/zhaofeng-shu33/pygncd)     |
| GN              |         |       | [![Travis (.com)](https://img.shields.io/travis/com/zhaofeng-shu33/Girvan-Newman)](https://travis-ci.com/zhaofeng-shu33/Girvan-Newman)      |
| pybhcd          | [![Build status](https://ci.appveyor.com/api/projects/status/64k0beoaddwkhcpy?svg=true)](https://ci.appveyor.com/project/zhaofeng-shu33/pybhcd)        |  [![Travis (.com)](https://img.shields.io/travis/com/zhaofeng-shu33/pybhcd)](https://travis-ci.com/zhaofeng-shu33/pybhcd)     |  [![CircleCI](https://circleci.com/gh/zhaofeng-shu33/pybhcd.svg?style=svg)](https://circleci.com/gh/zhaofeng-shu33/pybhcd)     |
| bhcd            | [![Build status](https://ci.appveyor.com/api/projects/status/keie9qh3oq9qes3a?svg=true)](https://ci.appveyor.com/project/zhaofeng-shu33/bhcd)        | [![Travis (.com)](https://img.shields.io/travis/com/zhaofeng-shu33/bhcd)](https://travis-ci.com/zhaofeng-shu33/bhcd)      | [![Travis (.com)](https://img.shields.io/travis/com/zhaofeng-shu33/bhcd)](https://travis-ci.com/zhaofeng-shu33/bhcd)      |
| bhcd-python-wrapper |      |      | [![Travis (.com)](https://img.shields.io/travis/com/zhaofeng-shu33/bhcd-python-wrapper)](https://travis-ci.com/zhaofeng-shu33/bhcd-python-wrapper)      |
| pspartition     |         |       |       |

## Experiment
Only Linux environment is supported.
| repository name     | Cloud   | self-hosted |
|---------------------|---------|-------------|
| data-clustering     |         |             |
| community-detection |         |             |
| outlier-detection   |         |             |
| nips-authorship     |         |             |
| pspartition-speed-compare   |         |         |